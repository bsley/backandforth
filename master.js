$( document ).ready(function() {

//set list of projects by their urls
var ProjectList = [
"reddesert.html",
"paristexas.html",
"lostintranslation.html",
"treeoflife.html"
];

//get total projects
var ProjectsTotal = ProjectList.length -1;

//get current path
var path = document.location.pathname;

//take the current path and make a string of the filename only
var file = path.substr(path.lastIndexOf('/')).substr(1);

//search for filename in array of projects and get index number
var CurrentProjectOrder = ProjectList.indexOf(file);

//build function to set hrefs with url variables
function ProjectNavPrev() {
	// check current project to ensure continous loop
	if (CurrentProjectOrder <= 0) {
		// if less than zero, return to last project
		var PrevProjectUrl = ProjectList[ProjectsTotal];
		$('#PrevProject').attr("href", PrevProjectUrl);
	}
	else if (CurrentProjectOrder > 0) {
		// set to previous project in array
		var PrevProjectUrl = ProjectList[CurrentProjectOrder -1];
		$('#PrevProject').attr("href", PrevProjectUrl);
	};        
};

function ProjectNavNext() {
	if (CurrentProjectOrder < ProjectsTotal) {
		//if less than the total, advance to project
		NextProjectUrl = ProjectList[CurrentProjectOrder +1];
		$("#NextProject").attr("href", NextProjectUrl);
	}
	else if (CurrentProjectOrder >= ProjectsTotal) {
		//if next project doesn't exist, return to first project
		NextProjectUrl = ProjectList[0];
		$("#NextProject").attr("href", NextProjectUrl);
	};   
};

//execute functions
ProjectNavPrev();
ProjectNavNext();

});